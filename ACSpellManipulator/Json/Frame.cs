﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class Frame
    {
        [JsonProperty("origin")]
        public Location Origin { get; set; }

        [JsonProperty("angles")]
        public Angles Angles { get; set; }

        public Frame()
        {
            Origin = new Location();
            Angles = new Angles();
        }
    }
}
