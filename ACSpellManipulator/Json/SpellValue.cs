﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ACSpellManipulator.Json
{
    public class SpellValue
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("school")]
        public byte Key { get; set; }

        [JsonProperty("iconID")]
        public long IconID { get; set; }

        [JsonProperty("category")]
        public int Category { get; set; }

        [JsonProperty("bitfield")]
        public int BitField { get; set; }

        [JsonProperty("base_mana")]
        public int BaseMana { get; set; }

        [JsonProperty("mana_mod")]
        public int ManaMod { get; set; }

        [JsonProperty("base_range_constant")]
        public float BaseRangeConstat { get; set; }

        [JsonProperty("base_range_mod")]
        public float BaseRangeMod { get; set; }

        [JsonProperty("power")]
        public int Power { get; set; }

        [JsonProperty("spell_economy_mod")]
        public float SpellEconomyMod { get; set; }

        [JsonProperty("component_loss")]
        public float ComponentLoss { get; set; }

        [JsonProperty("caster_effect")]
        public int CasterEffect { get; set; }

        [JsonProperty("target_effect")]
        public int TargetEffect { get; set; }

        [JsonProperty("fizzle_effect")]
        public int FizzleEffect { get; set; }

        [JsonProperty("recovery_interval")]
        public float RecoveryInterval { get; set; }

        [JsonProperty("recovery_amount")]
        public float RecoveryAmount { get; set; }

        [JsonProperty("display_order")]
        public int DisplayOrder { get; set; }

        [JsonProperty("non_component_target_type")]
        public int NonComponentTargetType { get; set; }

        [JsonProperty("formula_version")]
        public int FormulaVersion { get; set; }

        [JsonProperty("formula")]
        public List<int> Formula { get; set; }

        [JsonProperty("meta_spell")]
        public MetaSpell MetaSpell { get; set; }

        public SpellValue()
        {
            Formula = new List<int>(8);
            MetaSpell = new MetaSpell();
        }
    }
}
