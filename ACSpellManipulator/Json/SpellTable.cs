﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSpellManipulator.Json
{
    public class SpellTable
    {
        [JsonProperty("table")]
        public SpellBaseHash Spells { get; set; }

        public SpellTable()
        {
            Spells = new SpellBaseHash();
        }
    }
}
