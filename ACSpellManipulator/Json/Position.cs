﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class Position
    {
        [JsonProperty("objcell_id")]
        public long ObjectCellID { get; set; }

        [JsonProperty("frame")]
        public Frame Frame { get; set; }

        public Position()
        {
            Frame = new Frame();
        }
    }
}
