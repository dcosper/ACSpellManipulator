﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class Spell
    {
        public Spell(int type)
        {
            SPType = type;
        }
        [JsonIgnore]
        public int SPType { get; set; }

        [JsonProperty("spell_id")]
        public long SpellID { get; set; }

        [JsonProperty("duration")]
        public float Duration { get; set; }

        public bool ShouldSerializeDuration()
        {
            return SPType == 1 || SPType == 12;
        }

        [JsonProperty("degrade_modifier")]
        public float DegradeModifer { get; set; }

        public bool ShouldSerializeDegradeModifer()
        {
            return SPType == 1 || SPType == 12;
        }

        [JsonProperty("degrade_limit")]
        public float DegradeLimit { get; set; }

        public bool ShouldSerializeDegradeLimit()
        {
            return SPType == 1 || SPType == 12;
        }

        [JsonProperty("spellCategory")]
        public int SpellCategory { get; set; }

        public bool ShouldSerializeSpellCategory()
        {
            return SPType == 1 || SPType == 12;
        }


        [JsonProperty("smod")]
        public Smod Smod { get; set; }

        public bool ShouldSerializeSmod()
        {
            return SPType == 1 || SPType == 12;
        }

        [JsonProperty("etype")]
        public int Etype { get; set; }

        public bool ShouldSerializeEtype()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("baseIntensity")]
        public int BaseIntensity { get; set; }

        public bool ShouldSerializeBaseIntensity()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("variance")]
        public int Variance { get; set; }

        public bool ShouldSerializeVariance()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("wcid")]
        public long Wcid { get; set; }

        public bool ShouldSerializeWcid()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("numProjectiles")]
        public int NumberOfProjectiles { get; set; }

        public bool ShouldSerializeNumberOfProjectiles()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("numProjectilesVariance")]
        public float NumberOfProjectilesVariance { get; set; }

        public bool ShouldSerializeNumberOfProjectilesVariance()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("spreadAngle")]
        public float SpreadAngle { get; set; }

        public bool ShouldSerializeSpreadAngle()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("verticalAngle")]
        public float VerticalAngle { get; set; }

        public bool ShouldSerializeVerticalAngle()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("defaultLaunchAngle")]
        public float DefaultLaunchAngle { get; set; }

        public bool ShouldSerializeDefaultLaunchAngle()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("bNonTracking")]
        public int BNonTracking { get; set; }

        public bool ShouldSerializeBNonTracking()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("imbuedEffect")]
        public int ImbuedEffect { get; set; }

        public bool ShouldSerializeImbuedEffect()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("slayerCreatureType")]
        public int SlayerCreatureType { get; set; }

        public bool ShouldSerializeSlayerCreatureType()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("slayerDamageBonus")]
        public float SlayerDamageBonus { get; set; }

        public bool ShouldSerializeSlayerDamageBonus()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("critFreq")]
        public float CritFrequency { get; set; }

        public bool ShouldSerializeCritFrequency()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("critMultiplier")]
        public float CritMultiplier { get; set; }

        public bool ShouldSerializeCritMultiplier()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("ignoreMagicResist")]
        public int IgnoreMagicResist { get; set; }

        public bool ShouldSerializeIgnoreMagicResist()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("elementalModifier")]
        public float ElementalModifier { get; set; }

        public bool ShouldSerializeElementalModifier()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("createOffset")]
        public Location CreateOffset { get; set; }

        public bool ShouldSerializeCreateOffset()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("padding")]
        public Location Padding { get; set; }

        public bool ShouldSerializePadding()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("dims")]
        public Location Dims { get; set; }

        public bool ShouldSerializeDims()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("peturbation")]
        public Location Peturbation { get; set; }

        public bool ShouldSerializePeturbation()
        {
            return SPType == 2 || SPType == 10;
        }

        [JsonProperty("dt")]
        public int DT { get; set; }

        public bool ShouldSerializeDT()
        {
            return SPType == 3 || SPType == 11;
        }

        [JsonProperty("boost")]
        public int Boost { get; set; }

        public bool ShouldSerializeBoost()
        {
            return SPType == 3 || SPType == 11;
        }

        [JsonProperty("boostVariance")]
        public int BoostVariance { get; set; }

        public bool ShouldSerializeBoostVariance()
        {
            return SPType == 3 || SPType == 11;
        }

        [JsonProperty("src")]
        public int Src { get; set; }

        public bool ShouldSerializeSrc()
        {
            return SPType == 4;
        }

        [JsonProperty("dest")]
        public int Dest { get; set; }

        public bool ShouldSerializeDest()
        {
            return SPType == 4;
        }

        [JsonProperty("proportion")]
        public float Proportion { get; set; }

        public bool ShouldSerializeProportion()
        {
            return SPType == 4;
        }

        [JsonProperty("lossPercent")]
        public float LossPercent { get; set; }

        public bool ShouldSerializeLossPercent()
        {
            return SPType == 4;
        }

        [JsonProperty("sourceLoss")]
        public int SourceLoss { get; set; }

        public bool ShouldSerializeSourceLoss()
        {
            return SPType == 4;
        }

        [JsonProperty("transferCap")]
        public int TransferCap { get; set; }

        public bool ShouldSerializeTransferCap()
        {
            return SPType == 4;
        }

        [JsonProperty("maxBoostAllowed")]
        public int MaxBoostAllowed { get; set; }

        public bool ShouldSerializeMaxBoostAllowed()
        {
            return SPType == 4;
        }

        [JsonProperty("bitfield")]
        public int BitField { get; set; }

        public bool ShouldSerializeBitField()
        {
            return SPType == 4;
        }

        [JsonProperty("index")]
        public int Index { get; set; }

        public bool ShouldSerializeIndex()
        {
            return SPType == 5 || SPType == 6;
        }

        [JsonProperty("portal_lifetime")]
        public float PortalLifetime { get; set; }

        public bool ShouldSerializePortalLifetime()
        {
            return SPType == 7;
        }

        [JsonProperty("link")]
        public int Link { get; set; }

        public bool ShouldSerializeLink()
        {
            return SPType == 7;
        }

        [JsonProperty("pos")]
        public Position Position { get; set; }

        public bool ShouldSerializePosition()
        {
            return SPType == 8 || SPType == 13;
        }

        [JsonProperty("min_power")]
        public int MinPower { get; set; }

        public bool ShouldSerializeMinPower()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("max_power")]
        public int MaxPower { get; set; }

        public bool ShouldSerializeMaxPower()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("power_variance")]
        public float PowerVariance { get; set; }

        public bool ShouldSerializePowerVariance()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("school")]
        public int School { get; set; }

        public bool ShouldSerializeSchool()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("align")]
        public int Alignment { get; set; }

        public bool ShouldSerializeAlignment()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("number")]
        public int Number { get; set; }

        public bool ShouldSerializeNumber()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("number_variance")]
        public int NumberVariance { get; set; }

        public bool ShouldSerializeNumberVariance()
        {
            return SPType == 9 || SPType == 14;
        }

        [JsonProperty("drain_percentage")]
        public float DrainPercentage { get; set; }

        public bool ShouldSerializeDrainPercentage()
        {
            return SPType == 10;
        }

        [JsonProperty("damage_ratio")]
        public float DamageRatio { get; set; }

        public bool ShouldSerializeDamageRatio()
        {
            return SPType == 10;
        }

        public Spell()
        {
            Position = new Position();
            CreateOffset = new Location();
            Dims = new Location();
            Peturbation = new Location();
            Padding = new Location();
            Smod = new Smod();
        }
    }
}
