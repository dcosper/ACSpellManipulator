﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class Angles
    {
        [JsonProperty("w")]
        public float W { get; set; }

        [JsonProperty("x")]
        public float X { get; set; }

        [JsonProperty("y")]
        public float Y { get; set; }

        [JsonProperty("z")]
        public float Z { get; set; }
    }
}
