﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class Smod
    {
        [JsonProperty("key")]
        public int Key { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("val")]
        public float Value { get; set; }
    }
}
