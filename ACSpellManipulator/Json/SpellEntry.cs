﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSpellManipulator.Json
{
    public class SpellEntry
    {
        [JsonProperty("key")]
        public int Key { get; set; }

        [JsonProperty("value")]
        public SpellValue Value { get; set; }

        public SpellEntry()
        {
            Value = new SpellValue();
        }
        public override string ToString()
        {
            return Value.Name;
        }
    }
}
