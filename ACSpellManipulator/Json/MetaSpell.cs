﻿using Newtonsoft.Json;

namespace ACSpellManipulator.Json
{
    public class MetaSpell
    {
        [JsonIgnore]
        private int sptype;
        [JsonProperty("sp_type")] // i know dirty, bite me
        public int SpType { get { return sptype; } set { Spell = new Spell(value); sptype = value; } }

        [JsonProperty("spell")]
        public Spell Spell { get; set; }
    }
}
