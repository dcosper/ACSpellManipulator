﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ACSpellManipulator.Json
{

    public class SpellBaseHash
    {
        [JsonProperty("spellBaseHash")]
        public List<SpellEntry> SpellHashList { get; set; }

        public SpellBaseHash()
        {
            SpellHashList = new List<SpellEntry>();
        }
    }
}
