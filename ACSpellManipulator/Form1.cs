﻿using ACSpellManipulator.Json;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ACSpellManipulator
{
    public partial class Form1 : Form
    {
        private SpellTable CurrentSpellTable = null;

        public Form1()
        {
            InitializeComponent();
            comboBoxSpellList.SelectedIndexChanged += ComboBoxSpellList_SelectedIndexChanged;
        }

        private void ComboBoxSpellList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // crazy load he'a
            SpellEntry spellEntry = comboBoxSpellList.SelectedItem as SpellEntry;

            if(spellEntry != null)
            {
               // MessageBox.Show(spellEntry.Value.MetaSpell.SpType.ToString());
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Spells|spells.json";

            if(open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CurrentSpellTable = Newtonsoft.Json.JsonConvert.DeserializeObject<SpellTable>(File.ReadAllText(open.FileName));

                    LoadSpellTable();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, string.Format("Unable to open file {0}!", open.FileName));
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(CurrentSpellTable == null)
            {
                MessageBox.Show("Please open or create a spells.json");
                return;
            }

            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Spells|*.json";
            save.FileName = "spells.json";

            if (save.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(save.FileName, Newtonsoft.Json.JsonConvert.SerializeObject(CurrentSpellTable));
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Unable to save spell.json!");
                }
            }
            
        }
        void LoadSpellTable()
        {
            if (CurrentSpellTable == null) return;

            comboBoxSpellList.Items.Clear();

            foreach(SpellEntry se in CurrentSpellTable.Spells.SpellHashList)
            {
                comboBoxSpellList.Items.Add(se);
            }
            if (comboBoxSpellList.Items.Count > 0)
                comboBoxSpellList.SelectedIndex = 0;
        }

        private void dumpAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CurrentSpellTable == null) return;

            // pick a folder bish
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Dump all Spells to Folder.";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                // thread this
                foreach (SpellEntry se in CurrentSpellTable.Spells.SpellHashList)
                {
                    string fileName = Path.Combine(fbd.SelectedPath, string.Format("{0}.json", se.ToString()));

                    try
                    {
                        string data = Newtonsoft.Json.JsonConvert.SerializeObject(se);
                        File.WriteAllText(fileName, data);
                    }
                    catch (Exception) { }
                }
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // we import a spell
            if (CurrentSpellTable == null)
                CurrentSpellTable = new SpellTable();

            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Spell Dumps|*.json";
            open.Title = "Select a Spell dump file.";
            open.Multiselect = true;

            if(open.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in open.FileNames)
                {
                    try
                    {
                        SpellEntry se = Newtonsoft.Json.JsonConvert.DeserializeObject<SpellEntry>(File.ReadAllText(fileName), new JsonSerializerSettings { FloatParseHandling = FloatParseHandling.Double });

                        if (se != null)
                        {
                            if (!ContainsID(se.Key))
                            {
                                CurrentSpellTable.Spells.SpellHashList.Add(se);
                            }
                            else MessageBox.Show(string.Format("SpellEntry {0} found, aborting..", se.Key));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                LoadSpellTable();
            }
        }
        private bool ContainsID(long spellID)
        {
            if (CurrentSpellTable == null) return false;
            return CurrentSpellTable.Spells.SpellHashList.Any(entry => entry.Key == spellID);
        }
    }
}
